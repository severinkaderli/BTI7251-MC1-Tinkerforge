use std::{error::Error, io, thread};
use tinkerforge::{
    ip_connection::IpConnection, motorized_linear_poti_bricklet::*, segment_display_4x7_bricklet::*,
};

const HOST: &str = "localhost";
const PORT: u16 = 4223;
const UID: &str = "DCB";
const SEGMENT_UID: &str = "pRj";
const DIGITS: [u8; 16] = [
    0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71,
];

fn connect(host: &str, port: u16) -> IpConnection {
    let ip_connection = IpConnection::new();
    ip_connection.connect((host, port));
    ip_connection
}

fn main() -> Result<(), Box<dyn Error>> {
    let ip_connection = connect(HOST, PORT);
    let linear_poti = MotorizedLinearPotiBricklet::new(UID, &ip_connection);
    let segment_display = SegmentDisplay4x7Bricklet::new(SEGMENT_UID, &ip_connection);

    let position_receiver = linear_poti.get_position_callback_receiver();
    let position_receiver2 = linear_poti.get_position_callback_receiver();

    thread::spawn(move || {
        for position in position_receiver {
            let digit_100 = position % 1000 / 100;
            let digit_10 = position % 100 / 10;
            let digit_1 = position % 10;

            let segments = [
                DIGITS[0],
                DIGITS[digit_100 as usize],
                DIGITS[digit_10 as usize],
                DIGITS[digit_1 as usize],
            ];
            segment_display.set_segments(segments, 7, false);
        }
    });
    thread::spawn(move || {
        for position in position_receiver2 {
            println!("Position {}", position);
        }
    });

    // Set period for position callback to 0.05s (50ms) without a threshold.
    linear_poti.set_position_callback_configuration(50, true, 'x', 0, 100);

    println!("Press enter to exit.");
    let mut _input = String::new();
    io::stdin().read_line(&mut _input)?;
    ip_connection.disconnect();
    Ok(())
}
